\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{res}

\RequirePackage{titlesec}

\titleformat{\section}
    {\Large\scshape\raggedright}
    {}{0em}
    {}
    [\titlerule]

\newcommand{\datedsubsubsection}[2]{
    \subsubsection[#1]{#1 \hfill #2}
}

\newcommand{\datedsubsection}[2]{
    \subsection[#1]{#1 \hfill #2}
}

\newcommand{\name}[1]{
    \centerline{\Huge{#1}}
}

\newcommand{\contact}[2]{
    \centerline{#1 {\large\textperiodcentered} #2}
}
